﻿using iNFO.Areas.Account.Models;
using iNFO.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Net.Http;
using System.Text.Json.Serialization;
using System.Text;
using System;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Identity;
using iNFO.Areas.Master.Interface;
using iNFO.Helpers;
using iNFO.Areas.Master.Models;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Authentication;
using System.Security.Claims;

namespace iNFO.Areas.Account.Controllers
{
    [Area("Account")]
    [Route("account/login")]
    public class LoginController : Controller
    {
        static readonly HttpClient client = new HttpClient();
        private readonly IUserService userService;
        private readonly IOptions<Setting> setting;
        private readonly IMailService _mail;
        private string _uriLDAP { get; set; }
        private string _token { get; set; }
        private string _method { get; set; }
        private string _aplicationId { get; set; }

        public LoginController(
            IUserService userService,
            IOptions<Setting> setting,
            IMailService mail)
        {
            this.userService = userService;
            this.setting = setting;
            this._uriLDAP = this.setting.Value.LDAPConnection;
            this._token = this.setting.Value.Token;
            this._method = this.setting.Value.IsProduction ? "login" : "validate";
            this._aplicationId = this.setting.Value.ActiveSSO;
            _mail = mail;
        }

        [Route("")]
        [AllowAnonymous]
        // GET: LoginController
        public ActionResult Index(string returnUrl)
        {
            returnUrl ??= Url.Content("~/");
            ViewBag.ReturnUrl = returnUrl;

            return View();
        }

        [Route("on-post-ajax")]
        [HttpPost]
        public async Task<IActionResult> doLoginAjax([FromBody]Login login)
        {
            var sss = DBUtil.PasswordHash(login.Password);
            if (string.IsNullOrEmpty(login.Username) && string.IsNullOrEmpty(login.Password))
            {
                return Json(new { Success = false, Message = "Invalid login attempt.", UrlResponse = "" });
            }

            var user = await this.userService.FindUserByUsername(login.Username);
            if (user != null)
            {
                if (user.PASSWORD.Equals(DBUtil.PasswordHash(login.Password)))
                {
                    var claims = new List<Claim>
                    {
                        new Claim(ClaimTypes.Sid, user.USER_SID),
                        new Claim(ClaimTypes.Email, user.EMAIL),
                        new Claim(ClaimTypes.Name, user.FULLNAME),
                        new Claim(ClaimTypes.MobilePhone, user.PHONE_NUMBER),
                        new Claim(ClaimTypes.Surname, login.Username)

                    };
                    var claimsIdentity = new ClaimsIdentity(claims, "Login_iNFO");

                    await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claimsIdentity));

                    return Json(new { Success = true, Message = "Entering page!", UrlResponse = string.IsNullOrEmpty(ViewBag.ReturnUrl) ? Url.Content("~/") : ViewBag.ReturnUrl });
                }
            }

            return Json(new { Success = false, Message = "Username or password is incorrect.", UrlResponse = "" });

        }

        //private async Task<JsonResponse> LoginLDAP(LoginLDAP login)
        //{
        //    JsonResponse jsonResponse = new JsonResponse();
        //    try
        //    {
        //        client.DefaultRequestHeaders.Clear();
        //        client.DefaultRequestHeaders.Add("Token", this._token);

        //        var content = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
        //        using (var response = await client.PostAsync(this._uriLDAP, content))
        //        {
        //            string apiResponse = await response.Content.ReadAsStringAsync();
        //            // If hit API LDAP is success
        //            if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //            {
        //                jsonResponse = JsonConvert.DeserializeObject<JsonResponse>(apiResponse);
        //                jsonResponse.Success = jsonResponse.Status == "00" ? true : false;
        //            }
        //            else if (response.StatusCode == System.Net.HttpStatusCode.Unauthorized)
        //            {
        //                jsonResponse = JsonConvert.DeserializeObject<JsonResponse>(apiResponse);
        //                jsonResponse.Success = false;
        //            }
        //            else
        //            {
        //                jsonResponse = JsonConvert.DeserializeObject<JsonResponse>(apiResponse);
        //                jsonResponse.Success = false;
        //            }
        //        }

        //        return jsonResponse;
        //    }
        //    catch (Exception ex)
        //    {
        //        jsonResponse.Success = false; jsonResponse.Message = ex.Message;
        //        return jsonResponse;
        //    }
        //}

        private void setSession(UserDetail data)
        {
            try
            {
                var username = data.EMAIL.Split('@')[0];
                HttpContext.Session.SetString("userId", data.USER_IID.ToString());
                HttpContext.Session.SetString("fullname", data.FULLNAME);
                HttpContext.Session.SetString("email", data.EMAIL);
                HttpContext.Session.SetString("username", username);
                //HttpContext.Session.SetString("userType", data.IsPDSI ? "PDSI" : "Mitra");
            }
            catch (Exception ex)
            {
                Console.Write(ex);
                throw;
            }
        }

        //private async Task<LoginAPIResponse> LoginAPI(string username, string password)
        //{
        //    LoginAPIResponse apiresponse = new LoginAPIResponse();
        //    LoginAPIData dataLogin = new LoginAPIData();
        //    dataLogin.method = "Login";
        //    dataLogin.RememberMe = 0;
        //    dataLogin.UserName = "kairosdev";
        //    dataLogin.Password = "password";

        //    var data = new[]
        //    {
        //        new KeyValuePair<string, string>("method", "Login"),
        //        new KeyValuePair<string, string>("RememberMe", "0"),
        //        new KeyValuePair<string, string>("UserName", "kairosdev"),
        //        new KeyValuePair<string, string>("Password", "password"),
        //    };

        //    try
        //    {
        //        client.DefaultRequestHeaders.Clear();
        //        var content = new FormUrlEncodedContent(data);
        //        using (var response = await client.PostAsync(setting.Value.PDSI_Auth_Login, content))
        //        {
        //            string apiResponse = await response.Content.ReadAsStringAsync();
        //            // If hit API Login is success
        //            if (response.StatusCode == System.Net.HttpStatusCode.OK)
        //            {
        //                apiresponse = JsonConvert.DeserializeObject<LoginAPIResponse>(apiResponse);
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        apiresponse.message = ex.Message;
        //    }
        //    return apiresponse;
        //}
    }
}
