﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using System;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Authentication;

namespace iNFO.Areas.Account.Controllers
{
    [AllowAnonymous]
    [Area("Account")]
    public class LogoutController : Controller
	{

        public LogoutController()
        {
        }

        [Route("account/logout")]
        public async Task<IActionResult> doLogout()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Login");
        }
    }
}
