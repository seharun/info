﻿using iNFO.Areas.Master.Models;
using System.Threading.Tasks;

namespace iNFO.Areas.Master.Interface
{
	public interface IUserService
	{
        Task<UserDetail> FindUserByUsername(string Username);
    }
}
