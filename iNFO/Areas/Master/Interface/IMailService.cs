﻿using iNFO.Models;
using System.Threading.Tasks;

namespace iNFO.Areas.Master.Interface
{
	public interface IMailService
    {
        Task<MailResultModel> SendMail(MailModel mail);
    }
}
