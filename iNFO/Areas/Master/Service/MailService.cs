﻿using iNFO.Areas.Master.Interface;
using iNFO.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using System;

namespace iNFO.Areas.Master.Service
{
    public class MailService : IMailService
    {
        private static readonly HttpClient client = new HttpClient();
        private readonly IOptions<Setting> _setting;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly ISession _session;
        public MailService(IOptions<Setting> setting, IHttpContextAccessor httpContextAccessor)
        {
            _setting = setting;
            _httpContextAccessor = httpContextAccessor;
            _session = _httpContextAccessor.HttpContext.Session;
        }
        public async Task<MailResultModel> SendMail(MailModel mail)
        {
            MailResultModel apiresponse = new MailResultModel();
            LoginAPIData dataLogin = new LoginAPIData();

            var data = new[]
            {
                new KeyValuePair<string, string>("recipient", mail.recipient),
                new KeyValuePair<string, string>("subject", mail.subject),
                new KeyValuePair<string, string>("body", mail.body)
            };

            try
            {
                string token = _session.GetString("tokenapi");
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Add("Token", token);
                client.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
                var content = new FormUrlEncodedContent(data);
                using (var response = await client.PostAsync(_setting.Value.PDSI_Send_Mail, content))
                {
                    string apiResponse = await response.Content.ReadAsStringAsync();
                    // If hit API Login is success
                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        apiresponse = JsonConvert.DeserializeObject<MailResultModel>(apiResponse);
                    }
                }
            }
            catch (Exception ex)
            {
                apiresponse.Message = ex.Message;
            }
            return apiresponse;
        }
    }
}
