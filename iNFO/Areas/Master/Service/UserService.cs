﻿using Dapper;
using iNFO.Areas.Master.Interface;
using iNFO.Areas.Master.Models;
using iNFO.Data;
using System;
using System.Data;
using System.Threading.Tasks;

namespace iNFO.Areas.Master.Service
{
	public class UserService : IUserService
    {
        private readonly ConnectionDB _connectionDB;

        public UserService(ConnectionDB connectionDB)
        {
            _connectionDB = connectionDB;
        }
        public async Task<UserDetail> FindUserByUsername(string Username)
		{
            try
            {
                var data = new UserDetail();
                var param = new DynamicParameters (new{ Username });
                using (IDbConnection con = _connectionDB.Connection)
                {
                    data = await con.QueryFirstAsync<UserDetail>("[dbo].[sp_GetTblUsersByUsername]", param, commandType: System.Data.CommandType.StoredProcedure);
                    
                    con.Dispose();
                }

                return data;
            }
            catch (Exception e)
            {
                throw;
            }
        }
	}
}
