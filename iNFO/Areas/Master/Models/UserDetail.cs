﻿using System.ComponentModel.DataAnnotations;
using System;

namespace iNFO.Areas.Master.Models
{
    public class UserDetail
    {
        public string USER_SID { get; set; }
        public int USER_IID { get; set; }
        public string USER_UID { get; set; }
        public string FULLNAME { get; set; }
        public string PASSWORD { get; set; }
        public string EMAIL { get; set; }
        public string PHONE_NUMBER { get; set; }
        public DateTime POST_DATE { get; set; }
        public string POST_BY { get; set; }
        public DateTime MODF_DATE { get; set; }
        public string MODF_BY { get; set; }
    }
}
