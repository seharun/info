﻿using iNFO.Areas.Master.Models;
using iNFO.Data;
using Microsoft.AspNetCore.Antiforgery;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using System.Data;
using System.Threading.Tasks;
using System;
using Dapper;

namespace iNFO.Middlewares
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;

        public AuthenticationMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task InvokeAsync(
            HttpContext httpContext,
            ConnectionDB connectionDB,
            SignInManager<IdentityUser> signInManager,
            IAntiforgery antiforgery)
        {
            var path = httpContext.Request.Path;
            if (path.HasValue && path.Value.StartsWith("/account") == false)
            {
                if (httpContext.Session.GetString("userId") == null)
                {
                    if (signInManager.IsSignedIn(httpContext.User))
                    {
                        this.getUserLogin(httpContext, connectionDB, httpContext.Session.GetString("username"));
                    }
                }
            }

            await _next(httpContext);
        }

        private void getUserLogin(HttpContext httpContext, ConnectionDB _connectionDB, string userName)
        {
            UserDetail userDetail = new UserDetail();
            try
            {
                if (userName != null)
                {
                    using (IDbConnection conn = _connectionDB.Connection)
                    {
                        conn.Open();
                        userDetail = conn.QueryFirst<UserDetail>("sp_GetUserByUsername", new { Username = userName }, commandType: CommandType.StoredProcedure);
                        conn.Close();

                    }

                    if (!String.IsNullOrEmpty(userDetail.USER_IID.ToString()))
                    {
                        var username = userDetail.EMAIL.Split('@')[0];
                        httpContext.Session.SetString("userId", userDetail.USER_IID.ToString());
                        httpContext.Session.SetString("email", userDetail.EMAIL);
                        httpContext.Session.SetString("fullname", userDetail.FULLNAME);
                        httpContext.Session.SetString("username", username);
                        //httpContext.Session.SetString("role", userDetail.Role);
                    }
                }

            }
            catch (Exception ex)
            {
                System.Console.Write(ex.Message);
                throw;
            }
        }
    }

    public static class AuthenticationMiddlewareExtensions
    {
        public static IApplicationBuilder UseAuthenticationMiddlewareCustom(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<AuthenticationMiddleware>();
        }
    }
}
